<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('products', 'ProductController');
Route::resource('tugas', 'TasController');
Route::resource('uas', 'Tabel42Controller');
Route::get('/search', 'TasController@search');
Route::get('/find', 'ProductController@find');
