<!DOCTYPE html> <html>
<head>
<meta charset="utf-8">
<title>Penjualan Tas</title> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head> <body>
<div class="container">
<br />
@if (\Session::has('success'))
<div class="alert alert-success">
<p>{{ \Session::get('success') }}</p>
</div><br /> 
@endif
<form method="get" action="{{action('TasController@search')}}">
    <div class="row">
        <div class="col-md-4">
            <input type="text" class="form-control" name="search" placeholder="Masukkan nama barang">
        </div>
        <div class="col-md-2">
            <input type="submit" class="btn btn-secondary" value="Search!">
        </div>
    </div>
</form>
<br/>
<div class="row">
    <div class="col-lg-10">
        <h2>Daftar Barang</h2>
    </div>
    <div class="col-lg-2">
        <a href="{{action('TasController@create')}}" class="btn btn-success">Tambah Produk</a>
    </div>
</div>
<table class="table table-striped"> 
<thead>
    <tr>
        <th>ID</th>
        <th>Merk Tas</th> 
        <th>Harga Tas</th>
        <th>Ketersediaan</th>
        <th>Berat Tas</th>
        <th colspan="3">Action</th>
    </tr> 
</thead>
<tbody>
@foreach($tas as $t) 
<tr>
    <td>{{$t['id']}}</td>
    <td>{{$t['merktas']}}</td>
    <td align="right">{{number_format($t['hargatas'], 0)}}</td>
    @if ($t['tersedia']==1)
        <td>Tersedia</td>
    @elseif ($t['tersedia']==0)
        <td>Tidak Tersedia</td>
    @endif
    
    <td>{{$t['berat']}}</td>
    <td><a class="btn btn-info" href="{{action('TasController@show', $t['id'])}}">Detail</a></td>
    <td><a class="btn btn-warning" href="{{action('TasController@edit', $t['id'])}}">Ubah</a></td> 
    <td>
    <form action="{{action('TasController@destroy', $t['id'])}}" method="post">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="DELETE"> <button class="btn btn-danger" type="submit" onClick="return confirm('Anda yakin akan menghapus?')">Hapus</button>
    </form>
    </td>
</tr>
@endforeach 
</tbody>
{{ $tas->links() }}    
</table> 
</div> 

</body>
</html>