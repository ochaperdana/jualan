<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Penjualan Tas</title> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head> 
<body>
    <div class="container">
    <div class="row">
    <div class="col-md-10">
        <h2 style="text-align:center;">Penambahan Produk Tas</h2>
    </div>
    </div>
        <br/> 

        @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li> 
        @endforeach
        </ul> 
        </div>
        <br />
        @endif

        @if (\Session::has('success')) 
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p> 
        </div>
        <br />
        @endif

        <form method="post" action="{{url('tugas')}}">
        {{csrf_field()}}
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="merktas">Merk:</label> 
                    <input type="text" class="form-control" name="merktas"> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="hargatas">Harga:</label>
                        <input type="text" class="form-control" name="hargatas"> 
                    </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="tersedia">Tersedia:</label><br>
                        <!-- <select class="form-control" name="tersedia">
                            <option value="1">Tersedia</option>
                            <option value="0">Tidak Tersedia</option>
                        </select> -->
                        <input type="radio" name="tersedia" value="1"> Tersedia <br>
                        <input type="radio" name="tersedia" value="0"> Tidak Tersedia <br>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="berat">Berat:</label>
                        <input type="text" class="form-control" name="berat"> 
                    </div>
            </div> 
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-info" style="margin- left:38px">Tambahkan Produk</button>
                </div> 
            </div>
        </form> 
    </div>
</html>