<!DOCTYPE html> <html>
<head>
<meta charset="utf-8">
<title>Penjualan Tas</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head> <body>
<div class="container"> <h2>Detail Produk</h2><br /> 
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li> @endforeach
</ul> </div><br />
@endif
<form method="post" action="">
{{csrf_field()}}
<input name="_method" type="hidden" value="PATCH"> 
<div class="row">
    <div class="col-md-4"></div>
        <div class="form-group col-md-4">
            <label for="merktas">Merk:</label> 
            <input type="text" class="form-control" name="merktas" value="{{$tas->merktas}}" disabled> 
        </div> 
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="hargatas">Harga:</label>
            <input type="text" class="form-control" name="hargatas" value="{{$tas->hargatas}}" disabled> 
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="tersedia">Ketersediaan:</label>
            <select class="form-control" name="tersedia" disabled>
            @if ($tas->tersedia==1)
                <option value="1" selected>Tersedia</option>
                <option value="0">Tidak Tersedia</option>
            @elseif ($tas->tersedia==0)
                <option value="1">Tersedia</option>
                <option value="0" selected>Tidak Tersedia</option>
            @endif
            </select>
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="berat">Berat:</label>
            <input type="text" class="form-control" name="berat" value="{{$tas->berat}}" disabled>
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="created_at">Created At:</label>
            <input type="text" class="form-control" name="berat" value="{{$tas->created_at}}" disabled>
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="updated_at">Updated At:</label>
            <input type="text" class="form-control" name="berat" value="{{$tas->updated_at}}" disabled>
    </div>
</div> 
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4">
        <a href="{{action('TasController@index')}}" class="btn btn-danger" style="margin- left:38px">Kembali ke Halamnan Awal</a>
    </div> 
</div>
</form> 
</div>
</body> 
</html>