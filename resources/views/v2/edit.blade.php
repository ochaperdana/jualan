<!DOCTYPE html> <html>
<head>
<meta charset="utf-8">
<title>Penjualan Tas</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head> <body>
<div class="container"> <h2>Perubahan Produk</h2><br /> 
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li> @endforeach
</ul> </div><br />
@endif
<form method="post" action="{{action('TasController@update', $id)}}">
{{csrf_field()}}
<input name="_method" type="hidden" value="PATCH"> 
<div class="row">
    <div class="col-md-4"></div>
        <div class="form-group col-md-4">
            <label for="merktas">Merk:</label> 
            <input type="text" class="form-control" name="merktas" value="{{$tas->merktas}}"> 
        </div> 
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="hargatas">Harga:</label>
            <input type="text" class="form-control" name="hargatas" value="{{$tas->hargatas}}"> 
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="tersedia">Ketersediaan:</label><br>
            <!-- <select class="form-control" name="tersedia"> -->
            @if ($tas->tersedia==1)
                <!-- <option value="1" selected>Tersedia</option>
                <option value="0">Tidak Tersedia</option> -->
                <input type="radio" name="tersedia" value="1" checked> Tersedia <br>
                <input type="radio" name="tersedia" value="0"> Tidak Tersedia <br>
            @elseif ($tas->tersedia==0)
                <!-- <option value="1">Tersedia</option>
                <option value="0" selected>Tidak Tersedia</option> -->
                <input type="radio" name="tersedia" value="1"> Tersedia <br>
                <input type="radio" name="tersedia" value="0" checked> Tidak Tersedia <br>
            @endif
            <!-- </select> -->
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="berat">Berat:</label>
            <input type="text" class="form-control" name="berat" value="{{$tas->berat}}">
    </div>
</div> 
</div>

<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4">
        <button type="submit" class="btn btn-success" style="margin- left:38px">Update Produk</button>
    </div> 
</div>

</form> 
</div>
</body> 
</html>