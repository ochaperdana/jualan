<!DOCTYPE html> <html>
<head>
<meta charset="utf-8">
<title>Penjualan Tas</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head> <body>
<div class="container"> <h2>Detail Produk</h2><br /> 
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li> @endforeach
</ul> </div><br />
@endif
<form method="post" action="">
{{csrf_field()}}
<input name="_method" type="hidden" value="PATCH"> 
<div class="row">
    <div class="col-md-4"></div>
        <div class="form-group col-md-4">
            <label for="name">Nama Produk:</label> 
            <input type="text" class="form-control" name="name" value="{{$product->name}}" disabled> 
        </div> 
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="price">Harga:</label>
            <input type="text" class="form-control" name="price" value="{{$product->price}}" disabled> 
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="stok">Stok:</label>
            <input type="text" class="form-control" name="stok" value="{{$product->stok}}" disabled>
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="created_at">Created At:</label>
            <input type="text" class="form-control" name="created_at" value="{{$product->created_at}}" disabled>
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4"> 
            <label for="updated_at">Updated At:</label>
            <input type="text" class="form-control" name="updated_at" value="{{$product->updated_at}}" disabled>
    </div>
</div> 
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4">
        <a href="{{action('ProductController@index')}}" class="btn btn-danger" style="margin- left:38px">Kembali ke Halamnan Awal</a>
    </div> 
</div>
</form> 
</div>
</body> 
</html>