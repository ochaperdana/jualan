<!DOCTYPE html> <html>
<head>
<meta charset="utf-8">
<title>..:: Sistem Informasi Penjualan ::..</title> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head> <body>
<div class="container">
<br />
@if (\Session::has('success'))
<div class="alert alert-success">
<p>{{ \Session::get('success') }}</p>
</div><br /> 
@endif
<form method="get" action="{{action('ProductController@find')}}">
    <div class="row">
        <div class="col-md-4">
            <input type="text" class="form-control" name="search" placeholder="Masukkan nama barang">
        </div>
        <div class="col-md-2">
            <input type="submit" class="btn btn-secondary" value="Search!">
        </div>
    </div>
</form>
<div class="row">
    <div class="col-lg-10">
        <h2>Daftar Barang</h2>
    </div>
    <div class="col-lg-2">
        <a href="{{action('ProductController@create')}}" class="btn btn-success">Tambah Produk</a>
    </div>
</div>
<table class="table table-striped"> 
<thead>
    <tr>
        <th>ID</th>
        <th>Nama</th> 
        <th>Harga</th>
        <th>Stok</th>
        <th colspan="3">Action</th>
    </tr> 
</thead>
<tbody>
@foreach($products as $product) 
<tr>
    <td>{{$product['id']}}</td>
    <td>{{$product['name']}}</td>
    <td align="right">{{number_format($product['price'], 0)}}</td>
    <td>{{$product['stok']}}</td>
    <td><a class="btn btn-info" href="{{action('ProductController@show', $product['id'])}}">Detail</a></td>
    <td><a href="{{action('ProductController@edit', $product['id'])}}"
    class="btn btn-warning">Ubah</a></td> <td>
    <form action="{{action('ProductController@destroy', $product['id'])}}" method="post">
    {{csrf_field()}}
    <input name="_method" class="btn btn-danger" type="submit" value="Hapus">
    </form>
    </td>
</tr>
@endforeach 
</tbody>
{{ $products->links() }}
</table> 
</div> 

<script>
    $(".delete").on("submit", destroy(){
        return confirm("Are you sure?");
    });
</script>
</body>
</html>