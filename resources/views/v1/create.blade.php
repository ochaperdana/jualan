<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>..:: Sistem Informasi Penjualan ::..</title> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head> 
<body>
    <div class="container">
    <div class="row">
    <div class="col-md-10">
        <h2 style="text-align:center;">Penambahan Produk</h2>
    </div>
    </div>
        <br/> 

        @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li> 
        @endforeach
        </ul> 
        </div>
        <br />
        @endif

        @if (\Session::has('success')) 
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p> 
        </div>
        <br />
        @endif

        <form method="post" action="{{url('products')}}">
        {{csrf_field()}}
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="name">Nama:</label> 
                    <input type="text" class="form-control" name="name"> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="price">Harga:</label>
                        <input type="text" class="form-control" name="price"> 
                    </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="stok">Stok:</label>
                        <input type="text" class="form-control" name="stok"> 
                    </div>
            </div> 
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-success" style="margin- left:38px">Tambahkan Produk</button>
                </div> 
            </div>
        </form> 
    </div>
</html>