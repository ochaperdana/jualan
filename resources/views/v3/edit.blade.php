<!DOCTYPE html> <html>
<head>
<meta charset="utf-8">
<title>Penjualan Tas</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head> <body>
<div class="container"> <h2>Edit Tabel42 Kode {{$tabel42s->kode}}</h2><br /> 
@if ($errors->any())
<div class="alert alert-danger">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li> @endforeach
</ul> </div><br />
@endif
<form method="post" action="{{action('Tabel42Controller@update', $kode)}}">
{{csrf_field()}}
<input name="_method" type="hidden" value="PATCH"> 
<div class="row">
    <div class="col-md-4"></div>
        <div class="form-group col-md-4">
            <label for="namatabel42">Nama:</label> 
            <input type="text" class="form-control" name="namatabel42" value="{{$tabel42s->namatabel42}}"> 
        </div> 
</div>
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4" > 
            <label for="jumlahtabel42">Jumlah:</label>
            <input type="text" class="form-control" name="jumlahtabel42" value="{{$tabel42s->jumlahtabel42}}"> 
    </div>
</div> 
<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4" style="white-space:nowrap"> 
            <label for="satuantabel42">Satuan:</label><br>
            <select class="form-control" name="satuantabel42">
            @if ($tabel42s->satuantabel42==1)
                <option value="1" selected>Kilogram</option>
                <option value="2">Gram</option>
                <option value="3">Miligram</option>
                <!-- <input type="radio" name="tersedia" value="1" checked> Tersedia <br>
                <input type="radio" name="tersedia" value="0"> Tidak Tersedia <br> -->
            @elseif ($tabel42s->satuantabel42==2)
                <option value="1">Kilogram</option>
                <option value="2" selected>Gram</option>
                <option value="3">Miligram</option>
            @elseif ($tabel42s->satuantabel42==3)
                <option value="1">Kilogram</option>
                <option value="2" >Gram</option>
                <option value="3" selected>Miligram</option>
                <!-- <input type="radio" name="tersedia" value="1"> Tersedia <br>
                <input type="radio" name="tersedia" value="0" checked> Tidak Tersedia <br> -->
            @endif
            </select>
    </div>
</div> 

<div class="row">
    <div class="col-md-4"></div>
    <div class="form-group col-md-4">
        <button type="submit" class="btn btn-success" style="margin- left:38px">Simpan</button>
        <a href="{{action('Tabel42Controller@index')}}"><button type="button" class="btn btn-danger" style="margin- left:38px">Batal</button></a>
        <!-- <a href="{{action('Tabel42Controller@index')}}" type="submit" class="btn btn-danger" style="margin- left:38px">Batal</a> -->
    </div> 
    <!-- <div class="form-group col-md-4">
        <a href="{{action('Tabel42Controller@index')}}" type="submit" class="btn btn-danger" style="margin- left:38px">Batal</a>
    </div>  -->
</div>

</form> 
</div>
</body> 
</html>