<!DOCTYPE html> <html>
<head>
<meta charset="utf-8">
<title>Tabel 42</title> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head> <body>
<div class="container">
<br />
@if (\Session::has('success'))
<div class="alert alert-success">
<p>{{ \Session::get('success') }}</p>
</div><br /> 
@endif
<!-- <form method="get" action="{{action('TasController@search')}}">
    <div class="row">
        <div class="col-md-4">
            <input type="text" class="form-control" name="search" placeholder="Masukkan nama barang">
        </div>
        <div class="col-md-2">
            <input type="submit" class="btn btn-secondary" value="Search!">
        </div>
    </div>
</form>
<br/> -->
<div class="row">
    <div class="col-lg-10">
        <h2>Daftar Record Tabel42</h2>
    </div>
    <div class="col-lg-2">
        <a href="{{action('Tabel42Controller@create')}}" class="btn btn-success">Tambah Produk</a>
    </div>
</div>
<table class="table table-striped"> 
<thead>
    <tr>
        <th>No</th>
        <th>Kode</th> 
        <th>Nama</th>
        <th>Jumlah</th>
        <th>Satuan</th>
        <th colspan="3">Action</th>
    </tr> 
</thead>
<tbody>
@foreach($tabel42s as $i=>$t) 
<tr>
    <td>{{$i+1}}</td>
    <td>{{$t['kode']}}</td>
    <td>{{$t['namatabel42']}}</td>
    <td>{{$t['jumlahtabel42']}}</td>
    @if ($t['satuantabel42']==1)
        <td>Kilogram</td>
    @elseif ($t['satuantabel42']==2)
        <td>Gram</td>
    @elseif ($t['satuantabel42']==3)
        <td>Miligram</td>
    @endif
    
    <td><a class="btn btn-info" href="{{action('Tabel42Controller@edit', $t['kode'])}}">Ubah</a></td> 
</tr>
@endforeach 
</tbody>

</table> 
</div> 

</body>
</html>