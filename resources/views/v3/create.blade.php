<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title>Penjualan Tas</title> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head> 
<body>
    <div class="container">
    <div class="row">
    <div class="col-md-10">
        <h2 style="text-align:center;">Penambahan Produk Tas</h2>
    </div>
    </div>
        <br/> 

        @if ($errors->any())
        <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li> 
        @endforeach
        </ul> 
        </div>
        <br />
        @endif

        @if (\Session::has('success')) 
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p> 
        </div>
        <br />
        @endif

        <form method="post" action="{{url('uas')}}">
        {{csrf_field()}}
            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="kode">Kode:</label> 
                        <input type="text" class="form-control" name="kode"> 
                    </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <label for="namatabel42">Nama:</label> 
                    <input type="text" class="form-control" name="namatabel42"> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="jumlahtabel42">Jumlah:</label>
                        <input type="text" class="form-control" name="jumlahtabel42"> 
                    </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                    <div class="form-group col-md-4">
                        <label for="satuantabel42">Satuan:</label><br>
                        <select class="form-control" name="satuantabel42">
                            <option value="1">Kilogram</option>
                            <option value="2">Gram</option>
                            <option value="3">Miligram</option>
                        </select>
                        <!-- <input type="radio" name="tersedia" value="1"> Tersedia <br>
                        <input type="radio" name="tersedia" value="0"> Tidak Tersedia <br> -->
                    </div>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-info" style="margin- left:38px">Tambahkan Produk</button>
                    <a href="{{action('Tabel42Controller@index')}}"><button type="button" class="btn btn-danger" style="margin- left:38px">Batal</button></a>
                </div> 
            </div>
        </form> 
    </div>
</html>