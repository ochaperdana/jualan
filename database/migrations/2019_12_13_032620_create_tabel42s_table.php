<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTabel42sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabel42s', function (Blueprint $table) {
            $table->string('kode', 5)->primary();
            $table->string('namatabel42', 25);
            $table->integer('jumlahtabel42');
            $table->integer('satuantabel42');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tabel42s');
    }
}
