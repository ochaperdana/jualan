<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tabel42 extends Model
{
    protected $fillable=['kode','namatabel42', 'jumlahtabel42', 'satuantabel42'];
    protected $primaryKey = 'kode'; // or null

    public $incrementing = false;
    
}
