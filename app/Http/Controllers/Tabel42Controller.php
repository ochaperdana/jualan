<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tabel42;

class Tabel42Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabel42s= Tabel42::all()->toArray();
        return view('v3.index', \compact('tabel42s'), ['Tabel42' => $tabel42s]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('v3.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tabel42s=$this->validate(request(), [
            'kode' => 'required',
            'namatabel42' => 'required',
            'jumlahtabel42' => 'required|numeric', 
            'satuantabel42' => 'required']);
            Tabel42::create($tabel42s);
            return \redirect('uas')->with('success', 'Product has been added');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($kode)
    {
        $tabel42s=Tabel42::find($kode);
        return view('v3.edit', \compact('tabel42s','kode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tabel42s=Tabel42::find($id);
        $this->validate(request(), [
            'namatabel42' => 'required',
            'jumlahtabel42' => 'required|numeric',
            'satuantabel42' => 'required']
        );

            $tabel42s->namatabel42=$request->get('namatabel42');
            $tabel42s->jumlahtabel42=$request->get('jumlahtabel42');
            $tabel42s->satuantabel42=$request->get('satuantabel42');
            $tabel42s->save();
            return \redirect('uas')->with('success', 'Row has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
