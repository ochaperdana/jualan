<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tas;

class TasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$tas= Tas::all()->toArray();
        $tas=Tas::paginate(5);
        return view('v2.index', \compact('tas'), ['Tas' => $tas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('v2.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tas=$this->validate(request(), [
            'merktas' => 'required',
            'hargatas' => 'required|numeric', 
            'berat' => 'required|numeric',
            'tersedia' => 'required']);
            Tas::create($tas);
            return \redirect('tugas')->with('success', 'Product has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tas=Tas::find($id);
        return view('v2.show', \compact('tas','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tas=Tas::find($id);
        return view('v2.edit', \compact('tas','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tas=Tas::find($id);
        $this->validate(request(), [
            'merktas' => 'required',
            'hargatas' => 'required|numeric',
            'tersedia' => 'required', 
            'berat' => 'required|numeric']
        );

            $tas->merktas=$request->get('merktas');
            $tas->hargatas=$request->get('hargatas');
            $tas->berat=$request->get('berat');
            $tas->tersedia=$request->get('tersedia');
            $tas->save();
            return \redirect('tugas')->with('success', 'Product has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tas=Tas::find($id);
        $tas->delete();
        return \redirect('tugas')->with('success', 'Product has been deleted');
    }

    public function search(Request $request)
    {
        $cari=$request->search;
        $tas=Tas::where('merktas', 'like', "%" .$cari ."%")->paginate(2);
        return view('v2.index', \compact('tas'));
    }
}
