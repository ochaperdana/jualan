<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tas extends Model
{
    protected $fillable=['merktas', 'hargatas', 'berat', 'tersedia'];
}
